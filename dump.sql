--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.app_user (
    id integer NOT NULL,
    user_name character varying(32) NOT NULL,
    password character varying(32) NOT NULL,
    role character varying(32),
    salt character varying(255) NOT NULL
);


--
-- Name: app_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.app_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: app_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.app_user_id_seq OWNED BY public.app_user.id;


--
-- Name: pothole; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pothole (
    marker_id integer NOT NULL,
    lat character varying(32),
    long character varying(32),
    street_add character varying(100),
    size integer,
    depth integer,
    severity integer,
    report_date date,
    report_count integer,
    priority integer,
    is_repairing boolean,
    sent_for_repair date,
    admin_aware boolean,
    user_reported character varying(50)
);


--
-- Name: pothole_marker_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pothole_marker_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pothole_marker_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pothole_marker_id_seq OWNED BY public.pothole.marker_id;


--
-- Name: user_potholes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_potholes (
    user_id integer,
    marker_id character varying(32)
);


--
-- Name: app_user id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.app_user ALTER COLUMN id SET DEFAULT nextval('public.app_user_id_seq'::regclass);


--
-- Name: pothole marker_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pothole ALTER COLUMN marker_id SET DEFAULT nextval('public.pothole_marker_id_seq'::regclass);


--
-- Data for Name: app_user; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.app_user (id, user_name, password, role, salt) FROM stdin;
1	Glen	y8i5ywLfHE8mlK3N5V8lkA==	admin	CwDYMBc84B4A7LO4qrWbDQQ7elwa+/J+YkSxaC3e1vkcRmgWoxWE7Ea5pGZcK52su3r8mA/8K3nAS9xXaCNGTSNxamlgRbf2Rrai6KBUvORwLTlrEjso/D1PD/vCwHi/SuGWa2RMl8d01tbHKZe3TAYOb+8eiit4peQD6pSAvgk=
2	testadmin	y8i5ywLfHE8mlK3N5V8lkA==	admin	CwDYMBc84B4A7LO4qrWbDQQ7elwa+/J+YkSxaC3e1vkcRmgWoxWE7Ea5pGZcK52su3r8mA/8K3nAS9xXaCNGTSNxamlgRbf2Rrai6KBUvORwLTlrEjso/D1PD/vCwHi/SuGWa2RMl8d01tbHKZe3TAYOb+8eiit4peQD6pSAvgk=
3	hello	qLV6jdXNLEacpivbD7Y6Kw==	\N	4XQP9kZhaQFyaa3xhQBt0ViZzuF8M/BttInGZ2aCf4qMriMNatjtT6kmh9+Mvmq1lG/BE25LKvDTTz+W2kQvVwrWh/DsB36ov7SqIYVHvlGwIPHeOz8kCyLLZgz1xoTxSUtdNt20y5BuxaWgbtREm8kjQuQslwpBLsp9S1mD5Sg=
\.


--
-- Data for Name: pothole; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.pothole (marker_id, lat, long, street_add, size, depth, severity, report_date, report_count, priority, is_repairing, sent_for_repair, admin_aware, user_reported) FROM stdin;
1	41.52062865077371	-81.52718776116615	4254 Mayfield Rd, South Euclid, OH 44121, USA\t        	40	12	2	2018-12-20	1	0	\N	\N	\N	\N
2	41.51095049329637	-81.58002178041045	1792 Coventry Rd, Cleveland Heights, OH 44118, USA\t    	12	5	1	2018-12-20	4	0	\N	\N	\N	\N
3	41.48551381355208	-81.72452615049463	5417 Detroit Ave, Cleveland, OH 44102, USA\t            	40	12	2	2018-12-20	1	0	\N	\N	\N	\N
4	41.49881146422403	-81.67541366086374	2020b Carnegie Ave, Cleveland, OH 44115, USA\t        	40	12	2	2018-12-20	6	0	\N	\N	\N	\N
5	41.47694189158276	-81.72288270170634	Lorain Av & w 53rd St, Cleveland, OH 44102, USA\t    	24	5	1	2018-12-20	1	0	\N	\N	\N	\N
6	41.47334605784054	-81.70772585205077	3503 Wade Ave, Cleveland, OH 44113, USA\t            	12	5	1	2018-12-20	2	0	\N	\N	\N	\N
7	41.48721107016828	-81.66692438218524	Broadway & E 34th St, Cleveland, OH 44115, USA\t        	40	24	3	2018-12-20	1	0	\N	\N	\N	\N
8	41.48413876896496	-81.72971700082729	6415 Detroit Ave, Cleveland, OH 44102, USA\t            	24	5	1	2018-12-20	3	0	\N	\N	\N	\N
9	41.50128553496933	-81.65444175371522	4600 Carnegie Ave, Cleveland, OH 44103, USA\t        	24	24	2	2018-12-20	1	0	\N	\N	\N	\N
10	41.51599895868129	-81.68118323857271	2610 N Marginal Rd, Cleveland, OH 44114, USA\t        	12	12	1	2018-12-20	2	0	\N	\N	\N	\N
11	41.48351116909074	-81.74890200112793	9423 Clifton Blvd, Cleveland, OH 44102, USA\t        	24	24	2	2018-12-20	1	0	\N	\N	\N	\N
12	41.523075966674924	-81.64996790315547	6002 St Clair Ave, Cleveland, OH 44103, USA\t        	12	2	1	2018-12-20	1	0	\N	\N	\N	\N
13	41.47641023605422	-81.65141160715058	E 55th St & Bellford Av, Cleveland, OH 44127, USA\t    	40	24	3	2018-12-20	7	0	\N	\N	\N	\N
14	41.4806016389429	-81.78109130821247	13235 Franklin Blvd, Lakewood, OH 44107, USA\t        	24	5	1	2018-12-20	1	0	\N	\N	\N	\N
15	41.470066960945566	-81.7554996424044	3035 West Blvd, Cleveland, OH 44111, USA\t            	24	12	2	2018-12-20	1	0	\N	\N	\N	\N
16	41.4989371846153	-81.67481086658341	Cedar Av & E 22nd St, Cleveland, OH 44115, USA\t        	12	2	1	2018-12-20	10	0	\N	\N	\N	\N
17	41.47385807324429	-81.72287785492477	2116 W 53rd St, Cleveland, OH 44102, USA\t            	12	2	1	2018-12-20	1	0	\N	\N	\N	\N
18	41.46795005314277	-81.79765845092209	Warren Rd & Roxboro Av, Cleveland, OH 44111, USA\t    	12	12	1	2018-12-20	1	0	\N	\N	\N	\N
19	41.49781827949243	-81.70207468792842	1330 Old River Rd, Cleveland, OH 44113, USA\t        	40	24	3	2018-12-20	1	0	\N	\N	\N	\N
20	41.50703565199955	-81.6169598989868	1870 E 101st St, Cleveland, OH 44106, USA\t            	12	2	1	2018-12-20	1	0	\N	\N	\N	\N
21	41.462554993537815	-81.73882165351313	7303 Denison Ave, Cleveland, OH 44102, USA\t        	12	2	1	2018-12-20	1	0	\N	\N	\N	\N
22	41.46993721678411	-81.69184166269656	3100 W 14th St, Cleveland, OH 44109, USA\t            	24	12	2	2018-12-20	16	0	\N	\N	\N	\N
23	41.49150109686608	-81.62729895045402	2465 E 86th St, Cleveland, OH 44104, USA\t            	12	2	1	2018-12-20	1	0	\N	\N	\N	\N
24	41.452906042530515	-81.71646743803353	4441 Denison Ave, Cleveland, OH 44109, USA\t        	12	5	1	2018-12-20	1	0	\N	\N	\N	\N
25	41.48681833702456	-81.65017788377543	5701 Griswold Ave, Cleveland, OH 44104, USA\t        	40	24	3	2018-12-20	8	0	\N	\N	\N	\N
26	41.44741366584698	-81.76644462779586	11602 Bellaire Rd, Cleveland, OH 44111, USA\t        	12	2	1	2018-12-20	1	0	\N	\N	\N	\N
27	41.44511502644515	-81.60272412204449	Miles Av & E 116th St, Cleveland, OH 44105, USA\t    	12	2	1	2018-12-20	3	0	\N	\N	\N	\N
28	41.47769536385534	-81.7435514816039	8610 Madison Ave, Cleveland, OH 44102, USA\t            	24	5	1	2018-12-20	1	0	\N	\N	\N	\N
29	41.52957791539496	-81.59255107562257	1302 Lakefront Ave, Cleveland, OH 44108, USA\t        	12	12	1	2018-12-20	1	0	\N	\N	\N	\N
30	41.52939503521913	-81.59183744531356	1400 Lakefront Ave, Cleveland, OH 44108, USA\t        	24	24	2	2018-12-20	13	0	\N	\N	\N	\N
31	41.51719345873174	-81.61206488692841	1484 E 108th St, Cleveland, OH 44106, USA\t            	12	12	1	2018-12-20	1	0	\N	\N	\N	\N
32	41.53215562540741	-81.59673008507173	934 Eddy Rd, Cleveland, OH 44108, USA\t                	24	5	1	2018-12-20	1	0	\N	\N	\N	\N
33	41.518442327829376	-81.6094066426902	1437 E 110th St, Cleveland, OH 44106, USA\t            	12	2	1	2018-12-20	2	0	\N	\N	\N	\N
34	41.53307821680261	-81.60707970375142	884 Lakeview Rd, Cleveland, OH 44108, USA\t            	40	24	3	2018-12-20	1	0	\N	\N	\N	\N
35	41.47809094963797	-81.57668160367649	14900 S Woodland Rd, Shaker Heights, OH 44120, USA\t    	12	2	1	2018-12-20	1	0	\N	\N	\N	\N
36	41.529340869954346	-81.60179606314466	1019 E 123rd St, Cleveland, OH 44108, USA\t            	12	2	1	2018-12-20	12	0	\N	\N	\N	\N
37	41.54977546889784	-81.60200336387561	308 Eddy Rd, Cleveland, OH 44108, USA\t                	12	5	1	2018-12-20	1	0	\N	\N	\N	\N
38	41.528433422313874	-81.56362875482779	1060 Brandon Rd, Cleveland Heights, OH 44112, USA\t    	12	12	1	2018-12-20	1	0	\N	\N	\N	\N
39	41.46074010459334	-81.69399294189452	3400 Steelyard Dr, Cleveland, OH 44109, USA\t        	12	12	1	2018-12-20	5	0	\N	\N	\N	\N
40	41.462143335117126	-81.66451833850499	3461 Independence Rd, Cleveland, OH 44105, USA\t    	12	2	1	2018-12-20	1	0	\N	\N	\N	\N
41	41.5326742333602	-81.58705942407988	1680 Hayden Ave, East Cleveland, OH 44112, USA\t        	40	12	2	2018-12-20	4	0	\N	\N	\N	\N
42	41.474586201439166	-81.62822203721277	8282 OH-8, Cleveland, OH 44104, USA\t                	40	24	3	2018-12-20	1	0	\N	\N	\N	\N
43	41.475689652923236	-81.6301356248286	8127 Kinsman Rd, Cleveland, OH 44104, USA\t            	24	5	1	2018-12-20	2	0	\N	\N	\N	\N
44	41.4537935451805	-81.7012612904964	3723 OH-3, Cleveland, OH 44109, USA             \t    	12	12	1	2018-12-20	1	0	\N	\N	\N	\N
45	41.524877579598744	-81.51968999535552	1280 S Green Rd, South Euclid, OH 44121, USA  \t    	24	5	1	2018-12-20	3	0	\N	\N	\N	\N
46	41.52891231070708	-81.52236751215753	4387 Angela Dr, South Euclid, OH 44121, USA\t        	12	5	1	2018-12-20	1	0	\N	\N	\N	\N
47	41.519210951491395	-81.55278524769085	60 Severance Cir, Cleveland Heights, OH 44118, USA\t	24	12	2	2018-12-20	4	0	\N	\N	\N	\N
48	41.506131455379844	-81.61301046641643	1919 E 107th St, Cleveland, OH 44106, USA\t            	12	5	1	2018-12-20	1	0	\N	\N	\N	\N
49	41.53993555374504	-81.56269374213775	1990 Noble Rd, East Cleveland, OH 44112, USA\t        	12	2	1	2018-12-20	8	0	\N	\N	\N	\N
50	41.51774691661576	-81.54560217153005	1551 Crest Rd, Cleveland Heights, OH 44121, USA\t    	12	12	1	2018-12-20	9	0	\N	\N	\N	\N
51	41.51988965593699	-81.63379702078919	7805 Superior Ave, Cleveland, OH 44103, USA	12	2	1	2019-01-17	1	0	f	\N	f	hello
52	41.48620674888442	-81.82331118094544	Sloane Av & w Clifton Blvd, Lakewood, OH 44107, USA	40	12	2	2019-01-17	1	0	f	\N	f	hello
\.


--
-- Data for Name: user_potholes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.user_potholes (user_id, marker_id) FROM stdin;
\.


--
-- Name: app_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.app_user_id_seq', 3, true);


--
-- Name: pothole_marker_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.pothole_marker_id_seq', 52, true);


--
-- Name: app_user app_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT app_user_pkey PRIMARY KEY (id);


--
-- Name: app_user app_user_user_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT app_user_user_name_key UNIQUE (user_name);


--
-- Name: pothole pothole_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pothole
    ADD CONSTRAINT pothole_pkey PRIMARY KEY (marker_id);


--
-- PostgreSQL database dump complete
--

